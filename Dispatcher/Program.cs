﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace Dispatcher
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(() => SendMessage());
            while (true)
            {
                var dispatcher = new WorkerDispatcher();
                dispatcher.DispatchAsync(new RabbitMQListener());
            }
        }

        private static void SendMessage()
        {
            while (true)
            {
                var factory = new ConnectionFactory() { HostName = "localhost" };
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "hello",
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    string message = "Hello World!";
                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "",
                                         routingKey: "hello",
                                         basicProperties: null,
                                         body: body);
                    Console.WriteLine(" [->] Sent {0}", message);
                }
                Thread.Sleep(2000);
            }
        }
    }
}
