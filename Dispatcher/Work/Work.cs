﻿using System;

namespace Dispatcher
{
    public abstract class Work : IWork
    {
        private static int counter = 0;
        protected int currentNumber;
        protected static readonly Random RNG = new Random(DateTime.Now.Millisecond);

        protected Work()
        {
            currentNumber = counter++;
        }

        public abstract IWorkResult ProcessWork();
    }
}
