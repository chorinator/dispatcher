﻿namespace Dispatcher
{
    public interface IWork
    {
        IWorkResult ProcessWork();
    }
}
