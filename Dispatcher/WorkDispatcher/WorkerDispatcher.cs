﻿namespace Dispatcher
{
    public class WorkerDispatcher : Dispatcher
    {
        protected override void DoWork(IWork work)
        {
            var result = work.ProcessWork();
            result.PublishResult();
        }
    }
}
