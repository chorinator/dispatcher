﻿using System;
using System.Threading;

namespace Dispatcher
{
    public class RandomWorkGenerator : WorkGenerator
    {
        private static readonly Random rng = new Random(DateTime.Now.Millisecond);

        public override IWork ListenForWork()
        {
            Console.WriteLine("{0} Waiting for work on redis...", currentNumber);
            Thread.Sleep(rng.Next(1, 5) * 1000);
            Console.WriteLine("{0} Work assigned", currentNumber);

            return new SerializeRequest();
        }
    }
}
