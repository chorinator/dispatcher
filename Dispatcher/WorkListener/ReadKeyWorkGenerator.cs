﻿using System;

namespace Dispatcher
{
    public class ReadKeyWorkGenerator : WorkGenerator
    {
        public override IWork ListenForWork()
        {
            Console.WriteLine("{0} Waiting for work on redis...", currentNumber);
            var key = Console.ReadKey();
            Console.WriteLine("{0} Work assigned", currentNumber);

            return new SerializeRequest();
        }
    }
}
